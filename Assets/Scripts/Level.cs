﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    private void Awake()
    {
        //Just resetting the block count on a new level
        Block.blockCount = 0;
    }
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(Block.blockCount <= 0)
        {
            SceneLoader.LoadNextScene();
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    private GameSession myGameSession;

    private void Start()
    {
        //This was the original way of getting the GameSession
        //myGameSession = FindObjectOfType<GameSession>();
        //This is the improved way
        myGameSession = GameSession.Instance;
    }

    public static void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadTheNextScene()
    {
        LoadNextScene();
    }

    public void LoadStartScene()
    {
        myGameSession.ResetGame();
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

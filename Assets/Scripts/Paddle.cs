﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    private float screenWidth, paddleWidth, halfPaddle;
    private Camera myCamera;
    private Ball myBall;

	// Use this for initialization
	void Start () {
        myBall = FindObjectOfType<Ball>();
        //Get the width of the Paddle in pixels
        paddleWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        //Get a reference to the main Camera
        myCamera = Camera.main;
        //Calculate width of the screen in Unity Units
        screenWidth = myCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f)).x;
        //Variable to Cache half the paddlewith because the pivot point of the paddle is on the middle
        halfPaddle = paddleWidth / 2;
	}
	
	// Update is called once per frame
	void Update () {
        float paddleXposition;
        //Clamp the position from half the width of the paddle to the screen width minus half the width of the paddle
        paddleXposition = Mathf.Clamp(GetXPos(), halfPaddle, screenWidth- halfPaddle);
        //Update the paddle  position on every frame
        this.transform.position = new Vector3(paddleXposition, this.transform.position.y);
	}

    private float GetXPos()
    {
        //Get the x coordinate of the mouse position in Unity Units
        float mouseXPosition = myCamera.ScreenToWorldPoint(Input.mousePosition).x;
        if (GameSession.Instance.IsAutoPlayEnabled())
        {
            return myBall.transform.position.x;
        } else
        {
            return mouseXPosition;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    [SerializeField] Vector2 initialVelocity = new Vector2(2f, 15f);
    [SerializeField] AudioClip[] collisionSounds;
    [SerializeField] float randomFactor = 1f;

    Paddle myPaddle;
    float paddleHeight, ballHeight,offset;
    bool hasStarted;
    Rigidbody2D myRB2D;
    AudioSource myAudioSource;
    Queue<Vector2> ballCollisions;
    private int numberOfSameCollisions = 0;

	// Use this for initialization
	void Start () {
        ballCollisions = new Queue<Vector2>();
        myRB2D = GetComponent<Rigidbody2D>();
        myAudioSource = GetComponent<AudioSource>();
        hasStarted = false;
        myPaddle = GameObject.FindObjectOfType<Paddle>();
        paddleHeight = myPaddle.GetComponent<SpriteRenderer>().bounds.size.y;
        ballHeight = this.GetComponent<SpriteRenderer>().bounds.size.y;
        offset = (paddleHeight / 2) + (ballHeight / 2);
	}
	
	// Update is called once per frame
	void Update ()
    {
        LaunchBall();
    }

    private void LateUpdate()
    {
        if (!hasStarted)
        {
            LockBallInPosition();
        }
    }

    private void LockBallInPosition()
    {
        this.transform.position = new Vector3(myPaddle.transform.position.x, myPaddle.transform.position.y + offset, 0);
    }

    private void LaunchBall()
    {
        if (Input.GetMouseButtonDown(0) && !hasStarted)
        {
            hasStarted = true;
            myRB2D.velocity = initialVelocity;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ballCollisions.Count < 2)
        {
            ballCollisions.Enqueue(collision.GetContact(0).point);
        }
        else
        {
            //If more or equal to two collisions already and this third collision is equal to the first collision point in the queue
            if (CompareContactPoints(ballCollisions, collision.GetContact(0).point))
            {
                //Increase the counter of existent collisions
                numberOfSameCollisions++;
            }
            else //If more or equal to two collisions already but third collision is not in the queue
            {
                //Take the first collision in the queue out
                ballCollisions.Dequeue();
                //add this new collision to the queue
                ballCollisions.Enqueue(collision.GetContact(0).point);
                //Reset Queue and Collisions counter
                ResetBallCollisions();
            }
        }
        float xFinal, yFinal;
        if(numberOfSameCollisions > 1)
        {
            Vector2[] myCollisionPoints;
            myCollisionPoints = ballCollisions.ToArray();
            xFinal = (myCollisionPoints[1] - myCollisionPoints[0]).x;
            yFinal = (myCollisionPoints[1] - myCollisionPoints[0]).y;
            if(xFinal == 0f)
            {
                myRB2D.velocity += new Vector2(Random.Range(-randomFactor, randomFactor), 0);
            }
            else if (yFinal == 0)
            {
                myRB2D.velocity += new Vector2(0, Random.Range(-randomFactor, randomFactor));
            }
            ResetBallCollisions();
        }

        AudioClip clip = collisionSounds[Random.Range(0, collisionSounds.Length)];
        myAudioSource.PlayOneShot(clip);

        if (collision.gameObject.CompareTag("BreakableBlock"))
        {
            collision.gameObject.GetComponent<Block>().HandleHit();
        }
    }

    private void ResetBallCollisions()
    {
        ballCollisions.Clear();
        numberOfSameCollisions = 0;
    }

    private bool CompareContactPoints(Queue<Vector2> myContactPoints, Vector2 ContactPointToCompare)
    {
        foreach (Vector2 item in myContactPoints)
        {
            Vector2 comparison = ContactPointToCompare - item;
            if(comparison.magnitude <= 0.005)
            {
                return true;
            }
        }
        return false;
    }
}

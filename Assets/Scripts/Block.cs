﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    [SerializeField] AudioClip breakSound;
    [SerializeField] GameObject blockSparklesVFX;
    [SerializeField] Sprite[] hitSprites;

    public static int blockCount;

    private GameSession myGameSession;
    [SerializeField] private int timesHit = 0;

    private Camera myCamera;
	// Use this for initialization

	void Start () {
        if (gameObject.CompareTag("BreakableBlock"))
        {
            blockCount++;
        }
        myCamera = Camera.main;
        //original way of getting the GameSession
        //myGameSession = FindObjectOfType<GameSession>();

        //Improved Way
        myGameSession = GameSession.Instance;
    }

    // Update is called once per frame
    void Update () {
		
	}
 
    public void HandleHit()
    {
        timesHit++;
        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits)
        {
            DestroyBlock();
        } else
        {
            ShowNextHitSprite();
        }
    }

    private void ShowNextHitSprite()
    {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex] != null && timesHit <= hitSprites.Length)
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        } else
        {
            Debug.LogError("Block Sprite is missing from Array for "+gameObject.name);
        }
    }

    private void DestroyBlock()
    {
        PlayBlockDestroyedSFX();
        TriggerSparklesVFX();
        blockCount--;
        myGameSession.AddToScore();
        Destroy(gameObject);
    }

    private void PlayBlockDestroyedSFX()
    {
        AudioSource.PlayClipAtPoint(breakSound, myCamera.transform.position);
    }

    private void TriggerSparklesVFX()
    {
        GameObject sparkleVFX = Instantiate(blockSparklesVFX, transform.position, Quaternion.identity);
        Destroy(sparkleVFX, 2f);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameSession : MonoBehaviour {

    [Range(0,10)]
    [SerializeField] float gameSpeed = 1f;
    [SerializeField] int pointsPerBlockDestroyed = 10;
    [SerializeField] TextMeshProUGUI myScoreText;
    [SerializeField] bool isAutoPlayEnabled = false;

    private int currentScore = 0;

    private static GameSession _instance;

    //Getter for the GameSession
    public static GameSession Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogWarning("Attempted to access GameSession instance with no available instance.");//This should not happen unless I omit the script from a scene
            }
            return _instance;
        }
    }

    //Getter/Setter for the Score
    public int Score
    {
        get
        {
            return currentScore;
        }
        set
        {
            currentScore = Mathf.Max(value, 0);
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    /* this was the original method shown in the course
    private void Awake()
    {
        int gameSessionCount = FindObjectsOfType<GameSession>().Length;
        if(gameSessionCount > 1)
        {
            Destroy(gameObject);
        } else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
    */

    // Use this for initialization
    void Start () {
        UpdateScore();
    }
	
	// Update is called once per frame
	void Update () {
        Time.timeScale = gameSpeed;
	}


    public void AddToScore()
    {
        currentScore += pointsPerBlockDestroyed;
        UpdateScore();
    }

    public void UpdateScore()
    {
        myScoreText.text = currentScore.ToString();
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return isAutoPlayEnabled;
    }
}
